/*
 Navicat Premium Data Transfer

 Source Server         : PostgreSQL
 Source Server Type    : PostgreSQL
 Source Server Version : 120000
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : Schedule

 Target Server Type    : PostgreSQL
 Target Server Version : 120000
 File Encoding         : 65001

 Date: 16/03/2020 00:03:28
*/


-- ----------------------------
-- Table structure for Group
-- ----------------------------
DROP TABLE IF EXISTS "Schedule"."Group";
CREATE TABLE "Schedule"."Group" (
  "group_id" uuid NOT NULL,
  "group_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "group_tutor" uuid NOT NULL
)
;

-- ----------------------------
-- Records of Group
-- ----------------------------
INSERT INTO "Schedule"."Group" VALUES ('529e6f54-8c21-4570-afa6-d5b874235298', 'ORM-2', '744e24fc-e16f-44f1-8454-454448d45324');
INSERT INTO "Schedule"."Group" VALUES ('632d1ad8-32d8-421a-961b-a4cd6d40617f', 'ORM-1', '744e24fc-e16f-44f1-8454-454448d45324');
INSERT INTO "Schedule"."Group" VALUES ('e67719ea-8791-44bc-8f2c-e110152651e0', 'Himici', '0eb1e54c-0e8b-4fae-ac27-9066b7438dc2');
INSERT INTO "Schedule"."Group" VALUES ('16141738-7805-48c6-b5a5-1be6f3d4968a', 'Extra', '81efc59b-8608-4018-ba1a-43ad8f026574');

-- ----------------------------
-- Table structure for GroupStudents
-- ----------------------------
DROP TABLE IF EXISTS "Schedule"."GroupStudents";
CREATE TABLE "Schedule"."GroupStudents" (
  "group_id" uuid NOT NULL,
  "student_id" uuid NOT NULL
)
;

-- ----------------------------
-- Records of GroupStudents
-- ----------------------------
INSERT INTO "Schedule"."GroupStudents" VALUES ('529e6f54-8c21-4570-afa6-d5b874235298', '22b45076-f90f-43b9-9bc5-dcab2ce657cd');
INSERT INTO "Schedule"."GroupStudents" VALUES ('e67719ea-8791-44bc-8f2c-e110152651e0', '6755754a-715b-48e8-91f7-a701996e78f7');
INSERT INTO "Schedule"."GroupStudents" VALUES ('e67719ea-8791-44bc-8f2c-e110152651e0', '22b45076-f90f-43b9-9bc5-dcab2ce657cd');

-- ----------------------------
-- Table structure for Lesson
-- ----------------------------
DROP TABLE IF EXISTS "Schedule"."Lesson";
CREATE TABLE "Schedule"."Lesson" (
  "lesson_id" uuid NOT NULL,
  "lesson_tutor_id" uuid NOT NULL,
  "lesson_subject_id" uuid NOT NULL,
  "lesson_group_id" uuid NOT NULL,
  "lesson_week" int8 NOT NULL,
  "lessson_timetable_id" uuid NOT NULL
)
;

-- ----------------------------
-- Records of Lesson
-- ----------------------------
INSERT INTO "Schedule"."Lesson" VALUES ('d2500bc0-eb2e-4026-89e7-4cd7b0c84481', '744e24fc-e16f-44f1-8454-454448d45324', 'a7d54071-ac2c-4401-b816-848af1ea0e45', '529e6f54-8c21-4570-afa6-d5b874235298', 3, 'a926b4d0-11d5-4dec-8e87-f87842d00cbb');
INSERT INTO "Schedule"."Lesson" VALUES ('2e45116e-2524-4cac-8b58-555101cc9a67', '81efc59b-8608-4018-ba1a-43ad8f026574', 'fbc901a3-d27e-4c31-8657-7d00632e1aee', '632d1ad8-32d8-421a-961b-a4cd6d40617f', 2, '065f1021-98c5-454b-8c3c-eb8c731b295a');
INSERT INTO "Schedule"."Lesson" VALUES ('ab5b31b4-3c93-4644-ace9-db138c3f8e3a', '0eb1e54c-0e8b-4fae-ac27-9066b7438dc2', '5cac061f-49a8-461b-94b8-0f89ac9e7e3e', 'e67719ea-8791-44bc-8f2c-e110152651e0', 2, '8e0454fd-d375-4712-ad4d-b519a195cf38');

-- ----------------------------
-- Table structure for Student
-- ----------------------------
DROP TABLE IF EXISTS "Schedule"."Student";
CREATE TABLE "Schedule"."Student" (
  "student_id" uuid NOT NULL,
  "student_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "student_surname" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "student_age" int4
)
;

-- ----------------------------
-- Records of Student
-- ----------------------------
INSERT INTO "Schedule"."Student" VALUES ('22b45076-f90f-43b9-9bc5-dcab2ce657cd', 'Vasya', 'Andreevich', 16);
INSERT INTO "Schedule"."Student" VALUES ('6755754a-715b-48e8-91f7-a701996e78f7', 'Konstantin', 'Pavlovich', 17);
INSERT INTO "Schedule"."Student" VALUES ('b74b2853-9a0d-4ef6-bffe-fa6196d00630', 'Igor', 'Alexeevich', 16);
INSERT INTO "Schedule"."Student" VALUES ('48fa3125-dbae-4997-aa5b-20c7bc22a798', 'Vladimir', 'Ivanovivch', 18);

-- ----------------------------
-- Table structure for Subject
-- ----------------------------
DROP TABLE IF EXISTS "Schedule"."Subject";
CREATE TABLE "Schedule"."Subject" (
  "subject_id" uuid NOT NULL,
  "subject_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of Subject
-- ----------------------------
INSERT INTO "Schedule"."Subject" VALUES ('fbc901a3-d27e-4c31-8657-7d00632e1aee', 'Math');
INSERT INTO "Schedule"."Subject" VALUES ('5cac061f-49a8-461b-94b8-0f89ac9e7e3e', 'Physics');
INSERT INTO "Schedule"."Subject" VALUES ('a7d54071-ac2c-4401-b816-848af1ea0e45', 'Chemistry');

-- ----------------------------
-- Table structure for Timetable
-- ----------------------------
DROP TABLE IF EXISTS "Schedule"."Timetable";
CREATE TABLE "Schedule"."Timetable" (
  "timetable_id" uuid NOT NULL,
  "timetable_day_of_week" int2 NOT NULL,
  "timetable_time_of_lesson" time(6) NOT NULL
)
;

-- ----------------------------
-- Records of Timetable
-- ----------------------------
INSERT INTO "Schedule"."Timetable" VALUES ('065f1021-98c5-454b-8c3c-eb8c731b295a', 1, '08:00:00');
INSERT INTO "Schedule"."Timetable" VALUES ('a926b4d0-11d5-4dec-8e87-f87842d00cbb', 1, '09:30:00');
INSERT INTO "Schedule"."Timetable" VALUES ('8e0454fd-d375-4712-ad4d-b519a195cf38', 1, '13:00:00');

-- ----------------------------
-- Table structure for Tutor
-- ----------------------------
DROP TABLE IF EXISTS "Schedule"."Tutor";
CREATE TABLE "Schedule"."Tutor" (
  "tutor_id" uuid NOT NULL,
  "tutor_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "tutor_surname" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "tutor_subject_id" uuid
)
;

-- ----------------------------
-- Records of Tutor
-- ----------------------------
INSERT INTO "Schedule"."Tutor" VALUES ('744e24fc-e16f-44f1-8454-454448d45324', 'Pavel', 'Igorevich', 'fbc901a3-d27e-4c31-8657-7d00632e1aee');
INSERT INTO "Schedule"."Tutor" VALUES ('81efc59b-8608-4018-ba1a-43ad8f026574', 'Igor', 'Pavlovich', '5cac061f-49a8-461b-94b8-0f89ac9e7e3e');
INSERT INTO "Schedule"."Tutor" VALUES ('0eb1e54c-0e8b-4fae-ac27-9066b7438dc2', 'Egor', 'Olegovich', 'a7d54071-ac2c-4401-b816-848af1ea0e45');

-- ----------------------------
-- Primary Key structure for table Group
-- ----------------------------
ALTER TABLE "Schedule"."Group" ADD CONSTRAINT "Group_pkey" PRIMARY KEY ("group_id");

-- ----------------------------
-- Primary Key structure for table GroupStudents
-- ----------------------------
ALTER TABLE "Schedule"."GroupStudents" ADD CONSTRAINT "GroupStudents_pkey" PRIMARY KEY ("group_id", "student_id");

-- ----------------------------
-- Primary Key structure for table Lesson
-- ----------------------------
ALTER TABLE "Schedule"."Lesson" ADD CONSTRAINT "Lesson_pkey" PRIMARY KEY ("lesson_id");

-- ----------------------------
-- Primary Key structure for table Student
-- ----------------------------
ALTER TABLE "Schedule"."Student" ADD CONSTRAINT "Student_pkey" PRIMARY KEY ("student_id");

-- ----------------------------
-- Primary Key structure for table Subject
-- ----------------------------
ALTER TABLE "Schedule"."Subject" ADD CONSTRAINT "Subject_pkey" PRIMARY KEY ("subject_id");

-- ----------------------------
-- Primary Key structure for table Timetable
-- ----------------------------
ALTER TABLE "Schedule"."Timetable" ADD CONSTRAINT "Timetable_pkey" PRIMARY KEY ("timetable_id");

-- ----------------------------
-- Primary Key structure for table Tutor
-- ----------------------------
ALTER TABLE "Schedule"."Tutor" ADD CONSTRAINT "Tutor_pkey" PRIMARY KEY ("tutor_id");

-- ----------------------------
-- Foreign Keys structure for table Group
-- ----------------------------
ALTER TABLE "Schedule"."Group" ADD CONSTRAINT "FK_group_tutor" FOREIGN KEY ("group_tutor") REFERENCES "Schedule"."Tutor" ("tutor_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table GroupStudents
-- ----------------------------
ALTER TABLE "Schedule"."GroupStudents" ADD CONSTRAINT "GroupOfStudent" FOREIGN KEY ("student_id") REFERENCES "Schedule"."Student" ("student_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Schedule"."GroupStudents" ADD CONSTRAINT "GroupOfStudent_group" FOREIGN KEY ("group_id") REFERENCES "Schedule"."Group" ("group_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table Lesson
-- ----------------------------
ALTER TABLE "Schedule"."Lesson" ADD CONSTRAINT "FK_lesson_group" FOREIGN KEY ("lesson_group_id") REFERENCES "Schedule"."Group" ("group_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Schedule"."Lesson" ADD CONSTRAINT "FK_lesson_subject" FOREIGN KEY ("lesson_subject_id") REFERENCES "Schedule"."Subject" ("subject_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Schedule"."Lesson" ADD CONSTRAINT "FK_lesson_tutor" FOREIGN KEY ("lesson_tutor_id") REFERENCES "Schedule"."Tutor" ("tutor_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Schedule"."Lesson" ADD CONSTRAINT "FK_timetable" FOREIGN KEY ("lessson_timetable_id") REFERENCES "Schedule"."Timetable" ("timetable_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table Tutor
-- ----------------------------
ALTER TABLE "Schedule"."Tutor" ADD CONSTRAINT "FK_tutor_subject" FOREIGN KEY ("tutor_subject_id") REFERENCES "Schedule"."Subject" ("subject_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
