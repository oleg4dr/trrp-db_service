package model;

import database.tables.pojos.Group;
import database.tables.pojos.Subject;
import database.tables.pojos.Timetable;
import database.tables.pojos.Tutor;

public class PopulatedLesson {
    private int week;
    private Timetable timetable;
    private Group group;
    private Tutor tutor;
    private Subject subject;

    @Override
    public String toString() {
        return "PopulatedLesson{" +
                "week=" + week +
                ", timetable=" + timetable +
                ", group=" + group +
                ", tutor=" + tutor +
                ", subject=" + subject +
                '}';
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public PopulatedLesson() {
    }
}
