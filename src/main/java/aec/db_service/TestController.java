package aec.db_service;

import model.PopulatedLesson;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TestController {
    private final
    ServiceImplementation serviceImplementation;

    public TestController(ServiceImplementation serviceImplementation) {
        this.serviceImplementation = serviceImplementation;
    }

    @GetMapping("/test")
    public List<PopulatedLesson> test(@RequestParam int weekN){
        return serviceImplementation.getWeeklySchedule(weekN);
    }
}
