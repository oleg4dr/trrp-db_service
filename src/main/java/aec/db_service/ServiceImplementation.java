package aec.db_service;

import database.tables.pojos.Group;
import database.tables.pojos.Subject;
import database.tables.pojos.Timetable;
import database.tables.pojos.Tutor;
import model.PopulatedLesson;
import org.jooq.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static database.Tables.TIMETABLE;
import static database.tables.Group.GROUP;
import static database.tables.Lesson.LESSON;
import static database.tables.Subject.SUBJECT;
import static database.tables.Tutor.TUTOR;

@Service
public class ServiceImplementation {
    private final
    DSLContext create;

    public ServiceImplementation(DSLContext create) {
        this.create = create;
    }

    List<PopulatedLesson> getWeeklySchedule(int weekN){
        List<PopulatedLesson> res = new ArrayList<>();
         Result<Record> records =
                 create.select()
                        .from(LESSON, SUBJECT, GROUP, TUTOR, TIMETABLE)
                        .where(LESSON.LESSON_WEEK.eq((long) weekN))
                        .and(LESSON.LESSON_SUBJECT_ID.eq(SUBJECT.SUBJECT_ID))
                        .and(LESSON.LESSON_GROUP_ID.eq(GROUP.GROUP_ID))
                        .and(LESSON.LESSSON_TIMETABLE_ID.eq(TIMETABLE.TIMETABLE_ID))
                        .and(LESSON.LESSON_TUTOR_ID.eq(TUTOR.TUTOR_ID))
                        .fetch();
    for (Record r: records){
        PopulatedLesson obj = new PopulatedLesson();
        obj.setWeek(weekN);
        obj.setGroup(new Group(
                r.get(GROUP.GROUP_ID),
                r.get(GROUP.GROUP_NAME),
                r.get(GROUP.GROUP_TUTOR)));
        obj.setSubject(new Subject(
                r.get(SUBJECT.SUBJECT_ID),
                r.get(SUBJECT.SUBJECT_NAME)));
        obj.setTutor(new Tutor(
                r.get(TUTOR.TUTOR_ID),
                r.get(TUTOR.TUTOR_NAME),
                r.get(TUTOR.TUTOR_SURNAME),
                r.get(TUTOR.TUTOR_SUBJECT_ID)));
        obj.setTimetable(new Timetable(
                r.get(TIMETABLE.TIMETABLE_ID),
                r.get(TIMETABLE.TIMETABLE_DAY_OF_WEEK),
                r.get(TIMETABLE.TIMETABLE_TIME_OF_LESSON)));
        res.add(obj);
    }
        return res;
    }

}
