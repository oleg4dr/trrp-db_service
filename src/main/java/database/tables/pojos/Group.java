/*
 * This file is generated by jOOQ.
 */
package database.tables.pojos;


import java.io.Serializable;
import java.util.UUID;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Group implements Serializable {

    private static final long serialVersionUID = -166084887;

    private UUID   groupId;
    private String groupName;
    private UUID   groupTutor;

    public Group() {}

    public Group(Group value) {
        this.groupId = value.groupId;
        this.groupName = value.groupName;
        this.groupTutor = value.groupTutor;
    }

    public Group(
        UUID   groupId,
        String groupName,
        UUID   groupTutor
    ) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupTutor = groupTutor;
    }

    public UUID getGroupId() {
        return this.groupId;
    }

    public void setGroupId(UUID groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public UUID getGroupTutor() {
        return this.groupTutor;
    }

    public void setGroupTutor(UUID groupTutor) {
        this.groupTutor = groupTutor;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Group (");

        sb.append(groupId);
        sb.append(", ").append(groupName);
        sb.append(", ").append(groupTutor);

        sb.append(")");
        return sb.toString();
    }
}
