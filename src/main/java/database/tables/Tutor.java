/*
 * This file is generated by jOOQ.
 */
package database.tables;


import database.Indexes;
import database.Keys;
import database.Schedule;
import database.tables.records.TutorRecord;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row4;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tutor extends TableImpl<TutorRecord> {

    private static final long serialVersionUID = -895417924;

    /**
     * The reference instance of <code>Schedule.Tutor</code>
     */
    public static final Tutor TUTOR = new Tutor();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TutorRecord> getRecordType() {
        return TutorRecord.class;
    }

    /**
     * The column <code>Schedule.Tutor.tutor_id</code>.
     */
    public final TableField<TutorRecord, UUID> TUTOR_ID = createField(DSL.name("tutor_id"), org.jooq.impl.SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>Schedule.Tutor.tutor_name</code>.
     */
    public final TableField<TutorRecord, String> TUTOR_NAME = createField(DSL.name("tutor_name"), org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false), this, "");

    /**
     * The column <code>Schedule.Tutor.tutor_surname</code>.
     */
    public final TableField<TutorRecord, String> TUTOR_SURNAME = createField(DSL.name("tutor_surname"), org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false), this, "");

    /**
     * The column <code>Schedule.Tutor.tutor_subject_id</code>.
     */
    public final TableField<TutorRecord, UUID> TUTOR_SUBJECT_ID = createField(DSL.name("tutor_subject_id"), org.jooq.impl.SQLDataType.UUID, this, "");

    /**
     * Create a <code>Schedule.Tutor</code> table reference
     */
    public Tutor() {
        this(DSL.name("Tutor"), null);
    }

    /**
     * Create an aliased <code>Schedule.Tutor</code> table reference
     */
    public Tutor(String alias) {
        this(DSL.name(alias), TUTOR);
    }

    /**
     * Create an aliased <code>Schedule.Tutor</code> table reference
     */
    public Tutor(Name alias) {
        this(alias, TUTOR);
    }

    private Tutor(Name alias, Table<TutorRecord> aliased) {
        this(alias, aliased, null);
    }

    private Tutor(Name alias, Table<TutorRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Tutor(Table<O> child, ForeignKey<O, TutorRecord> key) {
        super(child, key, TUTOR);
    }

    @Override
    public Schema getSchema() {
        return Schedule.SCHEDULE;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.TUTOR_PKEY);
    }

    @Override
    public UniqueKey<TutorRecord> getPrimaryKey() {
        return Keys.TUTOR_PKEY;
    }

    @Override
    public List<UniqueKey<TutorRecord>> getKeys() {
        return Arrays.<UniqueKey<TutorRecord>>asList(Keys.TUTOR_PKEY);
    }

    @Override
    public List<ForeignKey<TutorRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<TutorRecord, ?>>asList(Keys.TUTOR__FK_TUTOR_SUBJECT);
    }

    public Subject subject() {
        return new Subject(this, Keys.TUTOR__FK_TUTOR_SUBJECT);
    }

    @Override
    public Tutor as(String alias) {
        return new Tutor(DSL.name(alias), this);
    }

    @Override
    public Tutor as(Name alias) {
        return new Tutor(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Tutor rename(String name) {
        return new Tutor(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Tutor rename(Name name) {
        return new Tutor(name, null);
    }

    // -------------------------------------------------------------------------
    // Row4 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row4<UUID, String, String, UUID> fieldsRow() {
        return (Row4) super.fieldsRow();
    }
}
